
# load the iris dataset as an example 
from sklearn.datasets import load_iris 
iris = load_iris() 
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
# store the feature matrix (X) and response vector (y) 
X = iris.data 
y = iris.target 
  
# splitting X and y into training and testing sets 
from sklearn.model_selection import train_test_split 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=1) 
  
# printing the shapes of the new X objects 
print(X_train.shape) 
print(X_test.shape) 
  
# printing the shapes of the new y objects 
print(y_train.shape) 
print(y_test.shape)

clf=tree.DecisionTreeClassifier()
clf.fit(X_train,y_train)

prediction=clf.predict(X_test)

print("predicted values=",prediction)
print("actual values=",y_test)


knn=KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train,y_train)

tree_predict=clf.predict(X_test)
neighbor_predict=knn.predict(X_test)

print("Accuracy of the decision tree classifier=",metrics.accuracy_score(y_test,tree_predict))
print("Accuracy of the K-Nearest Neighbor classifier=",metrics.accuracy_score(y_test,neighbor_predict))
